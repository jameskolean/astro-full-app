import type { APIRoute } from "astro";
let notes: Note[] = [{ title: "First Note", body: "This is my first note" }];

export const get: APIRoute = ({ params, request }) => {
  return {
    body: JSON.stringify({ notes }),
  };
};

export const post: APIRoute = async ({ request }) => {
  if (request.body) {
    const newNote = await request.json();
    const existingNote = notes.find((note) => note.title === newNote.title);
    if (!existingNote) {
      notes.push(newNote);
    }
  }
  return {
    body: JSON.stringify({ notes }),
  };
};

export const del: APIRoute = async ({ params, request }) => {
  const url = new URL(request.url);
  const title = url.searchParams.get("title");
  const newNotes = notes.filter((note) => note.title !== title);

  notes = newNotes;
  return {
    body: JSON.stringify({ notes }),
  };
};

export const put: APIRoute = async ({ request }) => {
  const editedNote = await request.json();
  const existingNoteIndex = notes.findIndex(
    (note) => note.title === editedNote.title
  );
  if (existingNoteIndex >= 0) {
    const newNotes = [
      ...notes.slice(0, existingNoteIndex),
      editedNote,
      ...notes.slice(existingNoteIndex + 1),
    ];
    notes = newNotes;
  } else {
    notes.push(editedNote);
  }
  return {
    body: JSON.stringify({
      notes,
    }),
  };
};

export const all: APIRoute = ({ request }) => {
  return {
    body: JSON.stringify({
      message: `This was a ${request.method}!`,
    }),
  };
};
