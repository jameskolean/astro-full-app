/// <reference types="astro/client" />

interface Note {
  title: string;
  body: string;
}
